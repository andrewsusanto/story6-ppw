from django import forms

class KegiatanForm(forms.Form):
    nama_kegiatan = forms.CharField(label='Nama Kegiatan', max_length=64,widget=forms.TextInput(attrs={'class':'form-control'}))

class PesertaForm(forms.Form):
    nama_peserta = forms.CharField(label='Nama Peserta', max_length=64, widget=forms.TextInput(attrs={'class':'form-control'}))